from django.db import models

# Create your models here.
class Role(models.Model):
    id = models.IntegerField(primary_key=True)
    role = models.CharField(max_length=10)
    class Meta:
        managed = False
        db_table = 'role'
    def __str__(self):
        return "{}".format(self.id)

class User(models.Model):
    id = models.IntegerField(primary_key=True)
    role = models.ForeignKey(Role, null=True, on_delete=models.SET_NULL)
    nama = models.TextField()
    divisi = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    foto = models.ImageField(upload_to="foto_user")
    posisi = models.CharField(max_length=30)
    class Meta:
        managed = False
        db_table = 'users'
    def __str__(self):
        return "{}".format(self.id)

class Kpi(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    jenis_kpi = models.CharField(max_length=50)
    bobot = models.CharField(max_length=10)
    target = models.CharField(max_length=10)
    class Meta:
        managed = False
        db_table = 'kpi'
    def __str__(self):
        return "{}".format(self.id)

class Review(models.Model):
    id = models.IntegerField(primary_key=True)
    kpi = models.ForeignKey(Kpi, null=True, on_delete=models.SET_NULL)
    pencapaian = models.CharField(max_length=10)
    score = models.CharField(max_length=10)
    berkas = models.CharField(max_length=100)
    status = models.CharField(max_length=20)
    class Meta:
        managed = False
        db_table = 'review'
    def __str__(self):
        return "{}".format(self.id)