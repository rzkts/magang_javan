from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from magang_kpi_javan import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^register/$', views.CreateUser, name="registers"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)